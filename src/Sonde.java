public class Sonde {
    private String nom, prenom, dateDeNaissance, telephone, idCaracteristique;
    private int numero;

    public Sonde(int numero, String nom, String prenom, String dateDeNaissance, String telephone, String idCaracteristique) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateDeNaissance = dateDeNaissance;
        this.telephone = telephone;
        this.idCaracteristique = idCaracteristique;
        this.numero = numero;
    }

    public String getDateDeNaissance() {
        return this.dateDeNaissance;
    }

    public void setDateDeNaissance(String dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public String getNom() {
        return this.nom;
    }

    public String getIdCaracteristique() {
        return idCaracteristique;
    }

    public void setIdCaracteristique(String idCaracteristique) {
        this.idCaracteristique = idCaracteristique;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
