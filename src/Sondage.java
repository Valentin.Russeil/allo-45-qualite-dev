public class Sondage {
    
    private String themeQuestionnaire;
    private String entreprise;

    public Sondage(String TQ, String entreprise) {
        this.themeQuestionnaire = TQ;
        this.entreprise = entreprise;
    }

    public String getThemeQuestionnaire() {
        return this.themeQuestionnaire;
    }

    public String getEntreprise() {
        return this.entreprise;
    }
}
