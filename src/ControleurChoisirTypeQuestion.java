import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurChoisirTypeQuestion implements EventHandler<ActionEvent> {

    private AppliAllo45 appli;
    private Questionnaire questionnaire;

    public ControleurChoisirTypeQuestion(AppliAllo45 appli,Questionnaire questionnaire){
        this.appli = appli;
        this.questionnaire=questionnaire;
    }

    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) (event.getSource());
        if (button.getText().contains("unique")){
            this.appli.modeConcepteurCreerQuestion(this.questionnaire,"u");
        }
        else if (button.getText().contains("multi")){
            this.appli.modeConcepteurCreerQuestion(this.questionnaire,"m");
        }
        else if (button.getText().contains("Libre")){
            this.appli.modeConcepteurCreerQuestion(this.questionnaire,"l");
        }
        else if (button.getText().contains("Classement")){
            this.appli.modeConcepteurCreerQuestion(this.questionnaire,"c");
        }
        else if (button.getText().contains("Notes")){
            this.appli.modeConcepteurCreerQuestion(this.questionnaire,"n");
        }
        
    }
    
}
