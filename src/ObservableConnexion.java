import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class ObservableConnexion implements ChangeListener<String> {
    private AppliConnexion appli;

    public ObservableConnexion(AppliConnexion appli) {
        this.appli = appli;
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        String user= this.appli.getUtilisateur();
        String mdps= this.appli.getMotDePasse();
        if(mdps=="" | user==""){
            this.appli.faireActiverBoutonConnexion(false);
        }
        else{
            this.appli.faireActiverBoutonConnexion(true);
        }
    }
}