public class TrancheAge {
    private String trancheMin, trancheMax;

    public TrancheAge(String trancheMin, String trancheMax){
        this.trancheMin = trancheMin;
        this.trancheMax = trancheMax;
    }

    public String getTrancheMin() {
        return trancheMin;
    }

    public void setTrancheMin(String trancheMin) {
        this.trancheMin = trancheMin;
    }

    public String getTrancheMax() {
        return trancheMax;
    }

    public void setTrancheMax(String trancheMax) {
        this.trancheMax = trancheMax;
    }
}
