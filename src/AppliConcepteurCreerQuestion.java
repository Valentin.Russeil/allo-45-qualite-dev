import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.BoundedRangeModel;

import javafx.application.Application;
import javafx.geometry.Insets;

public class AppliConcepteurCreerQuestion extends BorderPane  implements AppliConcepteur{
    private BorderPane centre;
    private List<String> listeQuestion;
    private Label typeQ;
    private String typei;
    private String entreprise;
    private TextField question;
    private TextField fieldAjoute;
    private TextField fieldMax;
    private boolean pret;
    private ComboBox<String> cConsulter;
    private AppliAllo45 appli;
    private Questionnaire questionnaire;
    private Question laQuestion;
    private TextField tfSupprimer;
    private ComboBox<String> cType;
    private Button bAjouter;
    private Button bSupprimer;

    public AppliConcepteurCreerQuestion(AppliAllo45 appli, Questionnaire questionnaire, String type){
        super();
        this.questionnaire = questionnaire;
        this.appli = appli;
        this.laQuestion = null;
        this.typei = type;
        this.creerQuestion();
    }

    public AppliConcepteurCreerQuestion(AppliAllo45 appli, Questionnaire questionnaire, Question laQuestion){
        super();
        this.questionnaire = questionnaire;
        this.appli = appli;
        this.laQuestion = laQuestion;
        this.typei = laQuestion.getType();
        this.creerQuestion();
        this.question.setText(this.laQuestion.getQuestion());
        if(this.typei.equals("n")){
            this.fieldMax.setText(laQuestion.getInfoSupp() + "");
        } else if (this.typei.equals("n")){
            this.fieldMax.setText(laQuestion.getInfoSupp() + "");
        }

        if(!this.typei.equals("n")){
            for(String rep : laQuestion.getLesReponses()){
                this.cConsulter.getItems().add(rep);
            }
            this.cConsulter.getSelectionModel().select(0);
        }
    }


    public String getEntreprsie(){
        return this.entreprise;
    }

    public Question getLaQuestion() {
        return laQuestion;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setEntreprise(String entreprise){
        this.entreprise=entreprise;
    }

    public String getTypeI() {
        return typei;
    }

    private void creerQuestion(){    
        this.bAjouter= new Button("AJOUTER");
        this.bSupprimer= new Button("SUPPRIMER");
        this.cType = new ComboBox<>();
        this.typeQ = new Label();
        this.question= new TextField();
        this.fieldAjoute= new TextField();
        this.pret=false;
        this.fieldMax = new TextField();
        this.cConsulter = new ComboBox<>();
        this.tfSupprimer = new TextField();
        //top
        VBox vTop= new VBox();
        BorderPane bTop= new BorderPane();
        HBox hRight= new HBox();
        HBox hLeft= new HBox();
        HBox hCentre= new HBox();
        HBox hBottom= new HBox();
        Label lType = new Label("Type : ");
        Text tEntreprise= new Text("Entreprise : ");
        Label lEntreprises= new Label(this.questionnaire.getClient().getRaisonSoc());
        Label questionnaire= new Label("Questionnaire : ");
        Label nomQuestionnaire= new Label(this.questionnaire.getNomQuestionnaire());
        Label laQuestion= new Label("La question : ");

        if(this.typei.equals("u")){
            typeQ.setText("Réponse unique");
        }
        else if(this.typei.equals("m")){
            typeQ.setText("Réponse multiple");
        }
        else if(this.typei.equals("l")){
            typeQ.setText("Réponse libre");
        }
        else if(this.typei.equals("n")){
            typeQ.setText("Note");
        }
        else if(this.typei.equals("c")){
            typeQ.setText("Classement");
        }

        hLeft.getChildren().addAll(tEntreprise,lEntreprises);
        bTop.setRight(hLeft);
        hRight.getChildren().addAll(lType, this.typeQ);
        bTop.setLeft(hRight);


        hCentre.getChildren().addAll(questionnaire,nomQuestionnaire);

        hBottom.getChildren().addAll(laQuestion,this.question);

        vTop.getChildren().addAll(bTop,hCentre,hBottom);

        bTop.setPadding( new Insets(10,10,10,10));
        hCentre.setPadding(new Insets(10));
        hBottom.setPadding(new Insets(10));
        vTop.setStyle("-fx-border-color: black ; -fx-border-width: 2px ;");
        vTop.setPadding(new Insets(20));
        VBox.setMargin(vTop, new Insets(10,20,20,20));
        

        //center
        if(this.typei.equals("u")){
            this.mettreFenetreUnique();
        }
        else if(this.typei.equals("m")){
            this.mettreFenetreMulti();
        }
        else if(this.typei.equals("l")){
            this.mettreFenetreLibre();
        }
        else if(this.typei.equals("n")){
            this.mettreFenetreNote();
        }
        else if(this.typei.equals("c")){
            this.mettreFenetreClassement();
        }
        //bottom
        BorderPane bottom= new BorderPane();
        Button enregistrer= new Button("ENREGISTRER");
        enregistrer.setOnAction(new ControleurEnregistrerQuestion(appli, this, this.laQuestion));
        bottom.setRight(enregistrer);

        //affichage
        this.setTop(vTop);
        this.setBottom(bottom);
        this.setPadding(new Insets(20));
        BorderPane.setMargin(bottom, new Insets(10,10,10,10));
        this.question.setMaxWidth(800);
        this.question.setMinWidth(800);

        this.bAjouter.setOnAction(new ControleurAjouterOuSupprimerReponse(this));
        this.bSupprimer.setOnAction(new ControleurAjouterOuSupprimerReponse(this));
    }

    public void mettreFenetreClassement(){
        this.setCenter(new QuestionClassement(this.appli, this.cType, this.fieldAjoute, this.cConsulter, this.fieldMax, this.bAjouter,this.bSupprimer));
    }

    public void mettreFenetreNote(){
        this.setCenter(new QuestionNote(this.fieldMax));
    }

    public void mettreFenetreUnique(){
        this.setCenter(new QuestionUnique(this.appli, this.cType, this.fieldAjoute, this.cConsulter, this.bAjouter,this.bSupprimer));
    }

    public void mettreFenetreLibre(){
        this.setCenter(new QuestionLibre(this.appli, this.cType, this.fieldAjoute, this.cConsulter, this.bAjouter,this.bSupprimer));
    }

    public void mettreFenetreMulti(){
        this.setCenter(new QuestionMulti(this.appli, this.cType, this.fieldAjoute, this.cConsulter, this.bAjouter,this.bSupprimer));
    }

    public void setTypei(String type){
        this.typei=type;
    }

    public String getQuestion(){
        return this.question.getText();
    }

    public String getReponse(){
        return this.fieldAjoute.getText();
    }

    public void ajouterLaReponse(String reponse){
        if(!this.cConsulter.getItems().contains(reponse)){
            this.cConsulter.getItems().add(reponse);
        }
            this.cConsulter.getSelectionModel().select(reponse);
    }

    public void supprimerLaQuestion(int num){
        this.listeQuestion.remove(num);
    }

    public String getTypeReponse(){
        return this.cType.getValue();
    }

    public int getInfoSupplementaire(){
        if(typei.equals("n") || typei.equals("c")){
            return Integer.parseInt(this.fieldMax.getText());
        }
        return -1;
    }

    public void effacerReponse() {
        this.fieldAjoute.clear();
    }

    public void supprimerElementComboBox(){
        String elem = this.cConsulter.getValue();
        this.cConsulter.getItems().remove(elem);
    }

    @Override
    public void retourArriere() {
        Optional<ButtonType> reponse = this.appli.popUpAvertissement("Êtes-vous sur de retourner en arrière ?", "Vous n'avez pas terminé votre questionnaire.").showAndWait();
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            this.appli.modeConcepteurChoisirTypeQuestion(questionnaire);
        }
        
    }

    public List<String> getLesReponses() {
        return this.cConsulter.getItems();
    }
}