import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;


public class AppliConcepteurChoisirTypeQuestion extends BorderPane implements AppliConcepteur{

    private AppliAllo45 appli;
    private Questionnaire questionnaire;

    /**
     * créer le graphe de scène et lance le jeu
     * 
     * @param stage la fenêtre principale
     */
    public AppliConcepteurChoisirTypeQuestion(AppliAllo45 appli,Questionnaire questionnaire) {
        super();
        this.appli = appli;
        this.questionnaire=questionnaire;
        ajouterCentre(this);
    }

    public void ajouterCentre(BorderPane root){
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(20));

        VBox unique = new VBox(10);
        unique.setAlignment(Pos.CENTER);
        unique.setPadding(new Insets(20));


        VBox classement = new VBox(10);
        classement.setAlignment(Pos.CENTER);
        classement.setPadding(new Insets(20));


        VBox notes = new VBox(10);
        notes.setAlignment(Pos.CENTER);
        notes.setPadding(new Insets(20));


        VBox libre = new VBox(10);
        libre.setAlignment(Pos.CENTER);
        libre.setPadding(new Insets(20));


        VBox multiple = new VBox(10);
        multiple.setAlignment(Pos.CENTER);
        multiple.setPadding(new Insets(20));

        

        ImageView uniqueImg = new ImageView(new Image("unique.jpg"));
        uniqueImg.setFitHeight(200);
        uniqueImg.setFitWidth(250);
        uniqueImg.setEffect(new DropShadow(10, Color.BLACK));
        Button uniqueBtn = new Button("Choix unique");
        uniqueBtn.setOnAction(new ControleurChoisirTypeQuestion(this.appli,this.questionnaire));

        unique.getChildren().addAll(uniqueImg, uniqueBtn);


        ImageView classementImg = new ImageView(new Image("classement.jpg"));
        classementImg.setFitHeight(200);
        classementImg.setFitWidth(250);
        classementImg.setEffect(new DropShadow(10, Color.BLACK));
        Button classementBtn = new Button("Classement");
        classementBtn.setOnAction(new ControleurChoisirTypeQuestion(this.appli,this.questionnaire));

        classement.getChildren().addAll(classementImg, classementBtn);


        ImageView notesImg = new ImageView(new Image("notes.jpg"));
        notesImg.setFitHeight(200);
        notesImg.setFitWidth(250);
        notesImg.setEffect(new DropShadow(10, Color.BLACK));
        Button notesBtn = new Button("Notes");
        notesBtn.setOnAction(new ControleurChoisirTypeQuestion(this.appli,this.questionnaire));


        notes.getChildren().addAll(notesImg, notesBtn);


        ImageView multipleImg = new ImageView(new Image("multiple.jpg"));
        multipleImg.setFitHeight(200);
        multipleImg.setFitWidth(250);
        multipleImg.setEffect(new DropShadow(10, Color.BLACK));
        Button multipleBtn = new Button("Choix multiple");
        multipleBtn.setOnAction(new ControleurChoisirTypeQuestion(this.appli,this.questionnaire));

        multiple.getChildren().addAll(multipleImg, multipleBtn);


        ImageView libreImg = new ImageView(new Image("libre.png"));
        libreImg.setFitHeight(200);
        libreImg.setFitWidth(250);
        libreImg.setEffect(new DropShadow(10, Color.BLACK));
        Button libreBtn = new Button("Libre");
        libreBtn.setOnAction(new ControleurChoisirTypeQuestion(this.appli,this.questionnaire));

        libre.getChildren().addAll(libreImg, libreBtn);


        Text instructions = new Text("Quel type de réponses\nsouhaitez vous mettre\ndans votre\nquestionnaire ?");
        instructions.setTextAlignment(TextAlignment.CENTER);
        instructions.setFont(Font.font("Arial", FontWeight.NORMAL, 24));
        


        gridpane.add(unique, 0, 0);
        gridpane.add(instructions, 1, 0);
        gridpane.setHalignment(instructions,HPos.CENTER);
        gridpane.setValignment(instructions,VPos.CENTER);

        gridpane.add(multiple, 2, 0);
        gridpane.add(classement, 0, 1);
        gridpane.add(notes, 1, 1);
        gridpane.add(libre, 2, 1);
        root.setCenter(gridpane);
    }

    @Override
    public void retourArriere() {
        this.appli.modeConcepteurAccueil();
    }
}