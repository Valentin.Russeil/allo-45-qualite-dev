import java.util.ArrayList;

import javafx.application.Application;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AppliSondeurAccueil extends VBox implements AppliSondeur {

    private TableView<Questionnaire> tableauSondage;
    private AppliAllo45 appli;
    private TableColumn colonneNbDeReponses;
    private TableColumn colonneQuestionnaire;
    private TableColumn colonneEntreprise;
    private Button buttonLancerSondage;

    
    public void center() {
        this.tableauSondage = new TableView<>();
        VBox partieGauche = new VBox();
        partieGauche.setSpacing(5);
        partieGauche.setPadding(new Insets(20));

        /**
         * Test d'insertion de donnée dans le TableView
         */
        // ObservableList<Sondage> data = FXCollections.observableArrayList(
        //     new Sondage("11/06/22", "14/06/22", "Couleurs", "Colour"),
        //     new Sondage("12/06/22", "12/06/22", "Avis sur Iphone", "Apple"),
        //     new Sondage("13/06/22", "13/06/22", "Avis sur les ordinateurs", "IUTO")
        // );

        /**
         * Création des colonnes du TableView
         */

        // colonneDateCreation.setCellValueFactory(new PropertyValueFactory<Sondage, String>("datedecréation"));

        // colonneDateModif.setCellValueFactory(new PropertyValueFactory<Sondage, String>("datedemodification"));
        this.colonneQuestionnaire = new TableColumn<>("Questionnaire");

        // colonneQuestionnaire.setCellValueFactory(new PropertyValueFactory<Sondage, String>("questionnaire"));
        this.colonneEntreprise = new TableColumn<>("Entreprise");
        this.colonneNbDeReponses = new TableColumn<>("Nombre de réponses");

        // colonneEntreprise.setCellValueFactory(new PropertyValueFactory<Sondage, String>("entreprise"));
        
        // this.tableauSondage.setItems(data);

        /**
         * Permet d'ajouter toutes les colonnes dans le TableView
         */

        this.tableauSondage.getColumns().addAll(colonneQuestionnaire, colonneEntreprise, this.colonneNbDeReponses);
        this.tableauSondage.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        colonneQuestionnaire.setMaxWidth( 1f * Integer.MAX_VALUE * 65 ); // 70% width
        colonneEntreprise.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); // 20% width
        colonneNbDeReponses.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // 15% width

        this.buttonLancerSondage = new Button("Lancer sondage");
        buttonLancerSondage.setTooltip(new Tooltip("Lance le sondage sur le questionnaire selectionné"));
        buttonLancerSondage.setOnAction(new ControleurCommencer(this.appli, this));
        partieGauche.getChildren().addAll(tableauSondage, buttonLancerSondage);

        VBox partieDroite = new VBox();
        partieDroite.setSpacing(5);
        partieDroite.setPadding(new Insets(20));
        /**
         * Test d'insertion de données
         */
        // ObservableList<Question> data2 = FXCollections.observableArrayList(
        //     new Question("11/06/22", "14/06/22", "Sur une échelle de 0 à 10\ncomment noteriez vous la\nla couleur des lampadaires\nde la ville ?", 15),
        //     new Question("12/06/22", "12/06/22", "Sur une échelle de 0 à 10\ncomment noteriez vous la\nla couleur de la marie ?", 19),
        //     new Question("13/06/22", "13/06/22", "Parmi ces couleurs classez en\ntrois pour repeindre la façade de\nla mairie", 12)
        // );

        /**
         * Création des colonnes du TableView
         */

        // colonneDateModif2.setCellValueFactory(new PropertyValueFactory<Sondage, String>("datedemodification"));
        // colonneQuestion.setCellValueFactory(new PropertyValueFactory<Question, String>("question"));
        // colonneNbDeReponses.setCellValueFactory(new PropertyValueFactory<Question, Integer>("nbreponses"));

        // this.tableauQuestionSondage.setItems(data2);

        /**
         * Permet d'ajouter toutes les colonnes dans le TableView
         */
                
        

    
        this.getChildren().add(partieGauche);
        this.getChildren().add(partieDroite);
        this.faireActiverBoutonLancer(false);
        this.tableauSondage.setOnMouseClicked(new ObservableSelectionQuestionnaireSondeur(appli, this));        
    }


    // public AppliSondeurAccueil(Application appli, List<Questionnaire> listeQuestionnaire) {TODO}

    // public void retourArriere() {TODO}

    public void actualiser() {
        this.afficherQuestionnaires();
    }

    public Questionnaire getQuestionnaireActuel() {
        return (Questionnaire) this.tableauSondage.getSelectionModel().selectedItemProperty().get();
    }

    // public Questionnaire getQuestionnaireMinReponses() {TODO}

    // public void mettreLesQuestions() {TODO}

    public void afficherQuestionnaires(){
        try {
            RequeteSQL requetes = new RequeteSQL(appli.getMysql());
            ArrayList<Questionnaire> repTabQuestionnaire = requetes.questionnaireSondeur();
            ObservableList<Questionnaire> ob = FXCollections.observableArrayList(repTabQuestionnaire);
            this.colonneQuestionnaire.setCellValueFactory(new PropertyValueFactory<Questionnaire, String>("nomQuestionnaire"));
            this.colonneEntreprise.setCellValueFactory(new PropertyValueFactory<Questionnaire, String>("client"));
            this.colonneNbDeReponses.setCellValueFactory(new PropertyValueFactory<Questionnaire, String>("nbReponses"));
            this.tableauSondage.setItems(ob);
        } catch (Exception e) {
            System.out.println("Attention:"+e.getMessage());
        }
    }

    public AppliSondeurAccueil(AppliAllo45 appli) {
        super();
        this.appli = appli;
        this.center();
        this.actualiser();
    }

    public void faireActiverBoutonLancer(boolean b) {
        this.buttonLancerSondage.setDisable(!b);
    }

    @Override
    public void retourArriere() {
        System.out.println("Rien");
    }
}