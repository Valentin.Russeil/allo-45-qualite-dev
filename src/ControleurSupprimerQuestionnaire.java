import java.util.Optional;

import org.w3c.dom.events.Event;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

public class ControleurSupprimerQuestionnaire implements EventHandler<ActionEvent>{
    
    private AppliAllo45 appliAllo45;
    private AppliConcepteurAccueil appliConcepteurAccueil;

    public ControleurSupprimerQuestionnaire(AppliAllo45 appliAllo45 ,AppliConcepteurAccueil appliConcepteurAccueil) {
        this.appliAllo45 = appliAllo45;
        this.appliConcepteurAccueil = appliConcepteurAccueil;
    }

    @Override
    public void handle(ActionEvent event) {
        Questionnaire questionnaire = this.appliConcepteurAccueil.getQuestionnaireActuel();
        Optional<ButtonType> reponse = this.appliAllo45.popUpAvertissement("Supprimer un questionnaire", "Voulez-vous vraiment vous supprimer la questionnaire " + questionnaire.getNomQuestionnaire() + " ?").showAndWait();
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            try {
                RequeteSQL requetes = new RequeteSQL(this.appliAllo45.getMysql());
                requetes.supprimerQuestionnaire(questionnaire);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            this.appliConcepteurAccueil.actualiser();
        }
    }
}
