import java.util.ArrayList;

public class Questionnaire {
    private int numero;
    private Client client;
    private String nomQuestionnaire;
    private ArrayList<Question> lesQuestions;
    private int numeroQ, numeroS;
    private String etat;
    private String nbReponses;
    private Panel panel;

    public Questionnaire(int numero, String nomQuestionnaire, String etat, Client client, Panel panel, String nbReponses){
        this.nbReponses = nbReponses;
        this.numero = numero;
        this.client = client;
        this.etat = etat;
        this.panel = panel;
        this.nomQuestionnaire = nomQuestionnaire;
        this.lesQuestions = new ArrayList<Question>();
    }

    public String getNomQuestionnaire() {
        return this.nomQuestionnaire;
    }

    public void setNomQuestionnaire(String nomQuestionnaire) {
        this.nomQuestionnaire = nomQuestionnaire;
    }

    public void ajouterQuestion(Question question){
        this.lesQuestions.add(question);
    }

    public void supprimerQuestion(Question question){
        this.lesQuestions.remove(question);
    }

    public Question getQuestionActuelle(){
        return this.lesQuestions.get(this.numeroQ);
    }

    public Question questionSuivante(){
        this.numeroQ += 1;
        return getQuestionActuelle();
    }

    public int getNumeroQ() {
        return this.numeroQ;
    }

    public void setNumeroQ(int numeroQ) {
        this.numeroQ = numeroQ;
    }

    public int getNumeroS() {
        return this.numeroS;
    }

    public void setNumeroS(int numeroS) {
        this.numeroS = numeroS;
    }

    public ArrayList<Question> getLesQuestions() {
        return this.lesQuestions;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getNumero() {
        return numero;
    }

    public String getNbReponses() {
        return nbReponses;
    }

    public void setNbReponses(String nbReponses) {
        this.nbReponses = nbReponses;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Panel getPanel() {
        return panel;
    }

    public void setPanel(Panel panel) {
        this.panel = panel;
    }

    public void ajouterSonde(Sonde sonde) {
        this.getPanel().getLesSondes().add(sonde);
    }
}
