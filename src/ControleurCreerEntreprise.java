import java.sql.SQLException;

import javax.xml.catalog.Catalog;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerEntreprise implements EventHandler<ActionEvent>{
    
    private AppliAllo45 appliAllo45;
    private AppliAttribuerEntrepriseNom attribuerEntrepriseNom;

    public ControleurCreerEntreprise(AppliAllo45 appliAllo45, AppliAttribuerEntrepriseNom attribuerEntrepriseNom) {
        this.appliAllo45 = appliAllo45;
        this.attribuerEntrepriseNom = attribuerEntrepriseNom;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            RequeteSQL requete = new RequeteSQL(this.appliAllo45.getMysql());
            String entreprise = this.attribuerEntrepriseNom.getTfEntreprise();
            Client ent = new Client(requete.nbMaxEntreprise() + 1, entreprise);
            this.attribuerEntrepriseNom.ajouterEntreprise(ent);
            this.attribuerEntrepriseNom.effacerReponse();
            requete.ajouterClient(ent);
        }
        catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
