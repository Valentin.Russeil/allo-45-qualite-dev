import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurQuestionSuivante implements EventHandler<ActionEvent>{

    AppliSondeurRepondreQuestion appli;

    public ControleurQuestionSuivante(AppliSondeurRepondreQuestion appli){
        this.appli=appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.questionSuivant();      
    }
    
}
