import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurCommencer implements EventHandler<ActionEvent>{

    private AppliAllo45 vueAllo;
    private AppliSondeurAccueil vueSondeurAccueil;

    public ControleurCommencer(AppliAllo45 vueAllo, AppliSondeurAccueil vueSondeurAccueil){
        this.vueAllo = vueAllo;
        this.vueSondeurAccueil = vueSondeurAccueil;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Questionnaire questionnaire = this.vueSondeurAccueil.getQuestionnaireActuel();
        this.vueAllo.modeSondeurRepondreQuestion(questionnaire);
    }

}
