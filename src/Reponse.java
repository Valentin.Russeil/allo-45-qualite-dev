public class Reponse {
    private String reponse;

    public Reponse(String reponse){
        this.reponse = reponse;
    }

    public String getReponse() {
        return this.reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }
}
