import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AppliAttribuerEntrepriseNom extends VBox implements AppliConcepteur {

    private Button bCreerEntreprise = new Button("Créer entreprise");
    private Button bCreerQuestionnaire = new Button("Créer Questionnaire");
    private ComboBox<Client>  cEntreprise = new ComboBox<>();
    private TextField tEntreprise = new TextField();
    private TextField tNomQuestionnaire = new TextField();
    private ComboBox<Panel> cPanel = new ComboBox<>();
    private AppliAllo45 appli;
    private RequeteSQL requetes;
    private Questionnaire questionnaire;

    // public init(){
    //     // super();
    //     // this.appli = appli;
    //     this.bCreerEntreprise= new Button("Créer entreprise");
    //     this.bCreerQuestionnaire= new Button("Créer questionnaire");
    //     this.cEntreprise= new ComboBox<>();
    //     this.tEntreprise= new TextField();
    //     this.tNomQuestionnaire= new TextField();
    // }
    
    public void EntrepriseNom(){
        HBox hEntreprise= new HBox();
        HBox hNomQuestionaire= new HBox();
        HBox hPanel = new HBox();
        BorderPane bBottom= new BorderPane();
        Label lEntreprises= new Label("Entreprise : ");
        Label lNomQuestionnaire= new Label("Nom Questionnaire : ");
        Label lPanel= new Label("Le panel : ");
        hEntreprise.getChildren().addAll(lEntreprises,this.cEntreprise,this.tEntreprise,this.bCreerEntreprise);
        hNomQuestionaire.getChildren().addAll(lNomQuestionnaire,this.tNomQuestionnaire);
        hPanel.getChildren().addAll(lPanel,this.cPanel);
        bBottom.setRight(this.bCreerQuestionnaire);
        bCreerQuestionnaire.setOnAction(new ControleurCreerOuModifierQuestionnaire(appli, this, this.questionnaire));
        this.bCreerEntreprise.setOnAction(new ControleurCreerEntreprise(appli, this));
        this.getChildren().addAll(hEntreprise,hNomQuestionaire,hPanel,bBottom);
        hEntreprise.setPadding(new Insets(10));
        hNomQuestionaire.setPadding(new Insets(10));
        hEntreprise.setSpacing(10);
        hNomQuestionaire.setSpacing(10);
        bBottom.setPadding(new Insets(20));
        hEntreprise.setAlignment(Pos.CENTER);
        hNomQuestionaire.setAlignment(Pos.CENTER);
        hPanel.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(0,10,0,20));
        

        ConnexionMySQL mysql = this.appli.getMysql();
        try{
            this.requetes = new RequeteSQL(mysql);
            for(Panel rep : this.requetes.lesPanels()){
                this.cPanel.getItems().addAll(rep);
            }
            this.cPanel.getSelectionModel().select(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try{
            for(Client rep : requetes.lesEntreprises()){
                this.cEntreprise.getItems().addAll(rep);
            }
            this.cEntreprise.getSelectionModel().select(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }   
    
    public String getCombobox(){
        return this.cEntreprise.getPromptText();
    }

    public String getTfEntreprise(){
        return this.tEntreprise.getText();
    }

    public void ajouterEntreprise(Client entreprise){
        this.cEntreprise.getItems().add(entreprise);
        this.cEntreprise.getSelectionModel().select(entreprise);
    }

    public void effacerReponse() {
        this.tEntreprise.clear();
    }

    public String getTfNomQuestionnaire(){
        return this.tEntreprise.getText();
    }

    public AppliAttribuerEntrepriseNom(AppliAllo45 appli, Questionnaire questionnaire) {
        super();
        this.appli = appli;
        this.questionnaire = questionnaire;
        this.EntrepriseNom();
        if(questionnaire != null){
            this.tNomQuestionnaire.setText(this.questionnaire.getNomQuestionnaire());
            this.cPanel.getSelectionModel().select(this.questionnaire.getPanel());
        }
    }

    public Questionnaire genererQuestionnaire() {
        int maxi = 0;
        try {
            maxi = this.requetes.nbMaxQuestionnaire();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new Questionnaire(maxi + 1, this.tNomQuestionnaire.getText(), "En construction", this.cEntreprise.getValue(), this.cPanel.getValue(), "0");
    }

    @Override
    public void retourArriere() {
        this.appli.modeConcepteurAccueil();
    }
}
