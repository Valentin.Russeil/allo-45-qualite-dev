import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;

public class SondeurMulti extends FlowPane implements FenetreTypesReponses{
   
    private List<String> reponses;
    private List<CheckBox> lesCheckBox;
    private ToggleGroup groupeReponse = new ToggleGroup();

    public SondeurMulti(Question question){
        super();
        this.reponses = question.getLesReponses();
        this.lesCheckBox = new ArrayList<>();

        for(String l:reponses){
            CheckBox cb = new CheckBox(l);
            lesCheckBox.add(cb);
            this.getChildren().add(cb);
        }  
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setAlignment(Pos.CENTER);
    }

    @Override
    public String getReponse(){
        String listeReponseSelectionne = "";
        for (CheckBox b: this.lesCheckBox){
            if (b.isSelected()) {
                listeReponseSelectionne += b.getText();
            } 
        }
        return listeReponseSelectionne+"\b ";
    } 
}
