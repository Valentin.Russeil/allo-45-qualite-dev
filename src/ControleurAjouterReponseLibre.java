import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurAjouterReponseLibre implements EventHandler<ActionEvent> {

    private SondeurLibre fenetre;

    public ControleurAjouterReponseLibre(SondeurLibre fenetre){
        this.fenetre = fenetre;
    }

    @Override
    public void handle(ActionEvent event) {
        fenetre.ajouterReponse();
        fenetre.effacerReponse();
    }
    
}
