public class Utilisateur {
    private String nom, prenom;
    private int role, idU;

    public Utilisateur(int idU, String nom, String prenom, int role){
        this.idU = idU;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
    }

    public int getIdU() {
        return this.idU;
    }

    public void setIdU(int idU) {
        this.idU = idU;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getRole() {
        return this.role;
    }

    public void setRole(int role) {
        this.role = role;
    }

}
