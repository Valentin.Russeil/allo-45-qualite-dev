public class Client {
    private String raisonSoc;
    private int num;

    public Client(int num, String raisonSoc){
        this.num = num;
        this.raisonSoc = raisonSoc;
    }

    public String getRaisonSoc() {
        return this.raisonSoc;
    }

    public void setRaisonSoc(String raisonSoc) {
        this.raisonSoc = raisonSoc;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString(){
        return this.getRaisonSoc();
    }
}
