import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class QuestionNote  extends HBox {
    public QuestionNote(TextField note){
        super();
        Text Note= new Text("Note Maximum : ");
        this.getChildren().addAll(Note,note);
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(20));
    }
}
