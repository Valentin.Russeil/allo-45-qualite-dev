import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurSondeSuivant implements EventHandler<ActionEvent>{

    AppliSondeurRepondreQuestion appli;

    public ControleurSondeSuivant(AppliSondeurRepondreQuestion appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) (event.getSource());
        this.appli.changerSonde();      
    }
    
}
