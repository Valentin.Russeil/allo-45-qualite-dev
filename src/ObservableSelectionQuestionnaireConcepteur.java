import javafx.event.EventHandler;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

public class ObservableSelectionQuestionnaireConcepteur implements EventHandler<MouseEvent>{

    private AppliAllo45 appli;
    private AppliConcepteurAccueil fenetre;

    public ObservableSelectionQuestionnaireConcepteur(AppliAllo45 appli, AppliConcepteurAccueil fenetre){
        this.appli = appli;
        this.fenetre = fenetre;
    }

    public void handle(MouseEvent event){
        Questionnaire questionnaire = this.fenetre.getQuestionnaireActuel();
        fenetre.afficherQuestions(questionnaire);
        fenetre.faireActiverBoutonQuestionnaire(true);
        fenetre.faireActiverBoutonQuestion(false);
        fenetre.desactiverBoutonPublierSelonCondition();
    }
}