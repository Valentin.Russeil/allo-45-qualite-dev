import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurActualiserConcepteur implements EventHandler<ActionEvent>{

    private AppliConcepteurAccueil accueil;

    public ControleurActualiserConcepteur(AppliConcepteurAccueil accueil){
        this.accueil = accueil;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        accueil.actualiser();
    }

}