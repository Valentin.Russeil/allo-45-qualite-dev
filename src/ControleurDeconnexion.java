import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;


public class ControleurDeconnexion implements EventHandler<ActionEvent>{
    private AppliAllo45 appli;
    
    public ControleurDeconnexion(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent arg0) {
        Optional<ButtonType> reponse = this.appli.popUpAvertissement("Deconnexion", "Voulez-vous vraiment vous déconnecter ?").showAndWait();
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            try {
                this.appli.getMysql().close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            this.appli.modeConnexion();
        }
    }

}
