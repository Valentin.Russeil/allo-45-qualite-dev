import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurActualiserSondeur implements EventHandler<ActionEvent>{

    private AppliSondeurAccueil accueil;

    public ControleurActualiserSondeur(AppliSondeurAccueil accueil){
        this.accueil = accueil;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        accueil.actualiser();
    }

}
