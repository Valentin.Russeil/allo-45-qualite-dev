import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurEnregistrerQuestion implements EventHandler<ActionEvent>{
    
    private AppliAllo45 appli;
    private AppliConcepteurCreerQuestion concepteurCreerQuestion;
    private Question q;

    public ControleurEnregistrerQuestion(AppliAllo45 appli, AppliConcepteurCreerQuestion concepteurCreerQuestion, Question q){
        this.appli = appli;
        this.concepteurCreerQuestion=concepteurCreerQuestion;
        this.q = q;
    }

    @Override
    public void handle(ActionEvent event) {
        int questionMax = 0;
        RequeteSQL requete = null;
        try{
            requete = new RequeteSQL(this.appli.getMysql());
        }
        catch(Exception e){
            System.out.println(e.getMessage());

        }

        try{
            questionMax = requete.nbMaxQuestion(this.concepteurCreerQuestion.getQuestionnaire());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }

        Question nouvelleQuestion = null;
        if(q == null) {
            if(this.concepteurCreerQuestion.getTypeI().equals("n") || this.concepteurCreerQuestion.getTypeI().equals("c") ){
                nouvelleQuestion = new Question(questionMax+1, this.concepteurCreerQuestion.getQuestion(), this.concepteurCreerQuestion.getTypeI(),this.concepteurCreerQuestion.getInfoSupplementaire());
            } else {
                nouvelleQuestion = new Question(questionMax+1, this.concepteurCreerQuestion.getQuestion(), this.concepteurCreerQuestion.getTypeI());
            }

            for(String rep : this.concepteurCreerQuestion.getLesReponses()){
                nouvelleQuestion.ajouterLaReponse(rep);
            }

            try{
                requete.enregistrerNouvelleQuestion(this.concepteurCreerQuestion.getQuestionnaire(), nouvelleQuestion);
            }
            catch(Exception e){
                System.out.println(e.getMessage());
            }
        } else {
            if(this.concepteurCreerQuestion.getTypeI().equals("n") || this.concepteurCreerQuestion.getTypeI().equals("c") ){
                nouvelleQuestion = new Question(this.q.getNumero(), this.concepteurCreerQuestion.getQuestion(), this.concepteurCreerQuestion.getTypeI(),this.concepteurCreerQuestion.getInfoSupplementaire());  
            } else {
                nouvelleQuestion = new Question(this.q.getNumero(), this.concepteurCreerQuestion.getQuestion(), this.concepteurCreerQuestion.getTypeI());  
            }

            for(String rep : this.concepteurCreerQuestion.getLesReponses()){
                nouvelleQuestion.ajouterLaReponse(rep);
            }

            try{
                requete.modifierUneQuestion(this.concepteurCreerQuestion.getQuestionnaire(), nouvelleQuestion);
            }
            catch(Exception e){
                System.out.println(e.getMessage());
            }
        }
        this.appli.modeConcepteurAccueil();
    }


}

