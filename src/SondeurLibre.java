import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


public class SondeurLibre extends HBox implements FenetreTypesReponses{
    private ComboBox<String> cmb;
    private Button boutonAjouter;
    private TextField champsAjouter;
    private AppliAllo45 appli;
    private Question question;
    private Questionnaire questionnaire;

    public SondeurLibre(Questionnaire questionnaire, Question question, AppliAllo45 appli){
        super(10);
        this.questionnaire = questionnaire;
        this.question = question;
        this.appli = appli;
        this.cmb = new ComboBox<>();
        this.boutonAjouter = new Button("Ajouter");
        this.boutonAjouter.setOnAction(new ControleurAjouterReponseLibre(this));
        this.champsAjouter = new TextField();
        this.getChildren().addAll(cmb,champsAjouter,boutonAjouter);
        this.setPadding(new Insets(20));
        this.setSpacing(10);
        cmb.getSelectionModel().select(0);
        this.setAlignment(Pos.CENTER);
        for(String rep : question.getLesReponses()){
            this.cmb.getItems().add(rep);
        }
        this.cmb.getSelectionModel().select(0);

        this.setPadding(new Insets(20));
        this.setAlignment(Pos.CENTER);
    }

    @Override
    public String getReponse(){
        return cmb.getValue();
    }

    public String getChampsAjouter(){
        return this.champsAjouter.getText();
        
    }

    public Alert erreurFormat(){
        Alert errorAlert = new Alert(Alert.AlertType.INFORMATION);
        errorAlert.setHeaderText("Champs non valide");
        errorAlert.setContentText("Veuillez mettre uniquement des lettres");
        return errorAlert;
    }

    public void ajouterReponse() {
        String rep = this.champsAjouter.getText();
        if(!this.cmb.getItems().contains(rep)){
            this.cmb.getItems().add(rep);
            try {
                RequeteSQL requetes = new RequeteSQL(this.appli.getMysql());
                requetes.ajouterReponseLibre(this.questionnaire, this.question, rep);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        this.cmb.getSelectionModel().select(rep);
        this.question.ajouterLaReponse(rep);
    }

    public void effacerReponse() {
        this.champsAjouter.clear();
    }
}
