import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRetourSondeur implements EventHandler<ActionEvent>{

    private AppliSondeur fenetre;

    public ControleurRetourSondeur(AppliSondeur fenetre){
        this.fenetre = fenetre;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        fenetre.retourArriere();
    }

}