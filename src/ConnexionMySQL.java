import java.sql.*;

public class ConnexionMySQL {
	private Connection mysql;
	private boolean connecte;
	public ConnexionMySQL() throws ClassNotFoundException{
		this.mysql=null;
		this.connecte=false;
		Class.forName("org.mariadb.jdbc.Driver");
	}

	public void connecter(String nomLogin, String motDePasse) throws SQLException {
        this.mysql=DriverManager.getConnection("jdbc:mysql://servinfo-mariadb:3306/DBrusseil",nomLogin,motDePasse);
        this.connecte=this.mysql!=null;
    }
	public void close() throws SQLException {
		// fermer la connexion
		this.mysql.close();
		this.connecte=false;
	}

    public boolean isConnecte() { return this.connecte;}

	public Statement createStatement() throws SQLException {
		return this.mysql.createStatement();
	}

	public PreparedStatement prepareStatement(String requete) throws SQLException{
		return this.mysql.prepareStatement(requete);
	}
	
}
