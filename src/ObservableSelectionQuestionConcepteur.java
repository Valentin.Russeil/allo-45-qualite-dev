import javafx.event.EventHandler;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

public class ObservableSelectionQuestionConcepteur implements EventHandler<MouseEvent>{

    private AppliAllo45 appli;
    private AppliConcepteurAccueil fenetre;

    public ObservableSelectionQuestionConcepteur(AppliAllo45 appli, AppliConcepteurAccueil fenetre){
        this.appli = appli;
        this.fenetre = fenetre;
    }

    public void handle(MouseEvent event){
        if (this.fenetre.getQuestionnaireActuel().getLesQuestions().size() != 0) {
            fenetre.faireActiverBoutonQuestion(true);
        }
        else {
            fenetre.faireActiverBoutonQuestion(false);
        }
    }
}