import java.util.ArrayList;
import java.util.List;

public class Question {
    
    private int numero;
    private String question;
    private String type;
    private int infoSupp;
    private List<String> lesReponses;
    private String reponseDonne;

    public Question(int numero, String question,String type) {
        this.numero = numero;
        this.question = question;
        this.type = type;
        this.lesReponses = new ArrayList<>();
        this.infoSupp =-1;
    }

    public Question(int numero, String question, String type, int infoSupp) {
        this.numero = numero;
        this.question = question;
        this.type = type;
        this.infoSupp = infoSupp;
        this.lesReponses = new ArrayList<>();
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String laQuestion) {
        this.question = laQuestion;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    public int getInfoSupp() {
        return infoSupp;
    }

    public void setInfoSupp(int infoSupp) {
        this.infoSupp = infoSupp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getLesReponses() {
        return this.lesReponses;
    }

    public void ajouterLaReponse(String rep) {
        this.lesReponses.add(rep);
    }

    public String getReponseDonne() {
        return reponseDonne;
    }

    public void setReponseDonne(String reponseDonne) {
        this.reponseDonne = reponseDonne;
    }
}

