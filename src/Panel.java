import java.util.ArrayList;
import java.util.List;

public class Panel{
    private String nom;
    private int num;
    private List<Sonde> lesSondes;

    public Panel(int num, String nom){
        this.num = num;
        this.nom = nom;
        this.lesSondes = new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return this.getNom();
    }

    public List<Sonde> getLesSondes() {
        return lesSondes;
    }

    public void setLesSondes(List<Sonde> lesSondes) {
        this.lesSondes = lesSondes;
    }

    public Sonde supprimerPremierSonde() {
        try {
            this.lesSondes.remove(0);
            return this.lesSondes.get(0);
        } catch (Exception e) {
            return null;
        }
    }
}
