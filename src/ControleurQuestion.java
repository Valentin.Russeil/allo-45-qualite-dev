import java.sql.SQLException;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

public class ControleurQuestion implements EventHandler<ActionEvent>{

    private AppliConcepteurAccueil appliConcepteurAccueil;
    private AppliAllo45 appliAllo45;

    public ControleurQuestion(AppliAllo45 appliAllo45, AppliConcepteurAccueil appliCA) {
        this.appliConcepteurAccueil = appliCA;
        this.appliAllo45 = appliAllo45;
    }

    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) (event.getSource());
        String txt = button.getText();
        Question question = this.appliConcepteurAccueil.getQuestionActuelle();
        Questionnaire questionnaire = this.appliConcepteurAccueil.getQuestionnaireActuel();
        if (txt.contains("Supprimer")) {
            Optional<ButtonType> reponse = this.appliAllo45.popUpAvertissement("Supprimer une question", "Voulez-vous vraiment vous supprimer la question " + question.getQuestion() + " ?").showAndWait();
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                try {
                    RequeteSQL requetes = new RequeteSQL(this.appliAllo45.getMysql());
                    requetes.supprimerQuestion(questionnaire, question);
                    this.appliConcepteurAccueil.actualiser();
                }
                catch (SQLException exception) {
                    System.out.println(exception.getMessage());
                }
            }
        }
        else {
            if (txt.contains("Modifier")) {
                this.appliAllo45.modeConcepteurCreerQuestion(questionnaire,question);
            }
            else{
                if (txt.contains("Créer")) {
                    this.appliAllo45.modeConcepteurChoisirTypeQuestion(this.appliConcepteurAccueil.getQuestionnaireActuel());
                }
            }
        }
    }
}