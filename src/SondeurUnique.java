import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;

public class SondeurUnique extends FlowPane implements FenetreTypesReponses{
   
    private List<String> reponses;
    private ToggleGroup groupeReponse = new ToggleGroup();
    private Set<RadioButton> lesRadiosButtons = new HashSet<>();

    public SondeurUnique(Question question){
        super();
        this.reponses = question.getLesReponses();
        ToggleGroup groupeReponse = new ToggleGroup();

        for( String l:reponses){
            RadioButton bt = new RadioButton(l);
            bt.setToggleGroup(groupeReponse);
            this.getChildren().add(bt);
            this.lesRadiosButtons.add(bt);
        }  

        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setAlignment(Pos.CENTER);
    }

    @Override
    public String getReponse(){
        for(RadioButton rb : this.lesRadiosButtons){
            if(rb.isSelected()){
                return rb.getText();
            }
        }
        return "";
    } 
}
