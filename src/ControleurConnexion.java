import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurConnexion implements EventHandler<ActionEvent>{
    private AppliConnexion appliCo;
    
    public ControleurConnexion(AppliConnexion appliCo){
        this.appliCo = appliCo;
    }

    @Override
    public void handle(ActionEvent arg0) {
        String utilisateur = this.appliCo.getUtilisateur();
        String motDePasse = this.appliCo.getMotDePasse();
        boolean co = this.appliCo.seConnecter(utilisateur, motDePasse);
        

        if(!co){
            this.appliCo.mettreErreurImpossibleConnexion();
        }
    }

}
