import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurModifierQuestionnaire implements EventHandler<ActionEvent>{
    
    private AppliAllo45 appliAllo45;
    private AppliConcepteurAccueil fenetre;
    
    public ControleurModifierQuestionnaire(AppliAllo45 appli, AppliConcepteurAccueil fenetre) {
        this.appliAllo45 = appli;
        this.fenetre = fenetre;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appliAllo45.modeCreerQuestionnaire(this.fenetre.getQuestionnaireActuel());
    }
}
