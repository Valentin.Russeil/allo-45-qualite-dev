import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class sondeurNote extends VBox implements FenetreTypesReponses{
    private Slider slider;
    private Label afficheNote;
    private Button boutonMoins;
    private Button boutonPlus;

    @Override
    public String getReponse(){
        double valeurSliderDouble =slider.getValue();
        int valeurSliderInt = (int) valeurSliderDouble;      
        return Integer.toString(valeurSliderInt);
    }

    public Slider getSlider(){   
        return slider;
    }

    public sondeurNote(Question question){
        super(10);
        this.slider = new Slider();
        this.afficheNote = new Label("Votre Note : 5");
        this.boutonMoins = new Button("-");
        this.boutonPlus = new Button("+");
        
        HBox zoneSlider = new HBox();
        

        slider.setMin(0);
        slider.setMax(question.getInfoSupp());
        slider.setValue(question.getInfoSupp() / 2);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        
        slider.setBlockIncrement(1);
        slider.valueProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable,Number oldValue, Number newValue){
                int note = newValue.intValue();
                afficheNote.setText("Votre Note : " + note);
            }
        });

        zoneSlider.getChildren().addAll(boutonMoins,slider,boutonPlus);
        this.getChildren().addAll(zoneSlider,afficheNote);
        boutonMoins.setOnAction(new ControlleurBouton(this, 1));
        boutonPlus.setOnAction(new ControlleurBouton(this, 2));
        zoneSlider.setPadding(new Insets(20));
        zoneSlider.setSpacing(10);
        this.setAlignment(Pos.CENTER);
        zoneSlider.setAlignment(Pos.CENTER);

        this.setPadding(new Insets(20));
        this.setAlignment(Pos.CENTER);
    }

    public String getNote() {
        double valeurSliderDouble = slider.getValue();
        return "" + (int) valeurSliderDouble;
    }
}