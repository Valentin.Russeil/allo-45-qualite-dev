import java.awt.Dimension;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class AppliAllo45 extends Application{
    private BorderPane fenetre;
    private Button bRetour;
    private Button bActualiser;
    private Button bDeconnexion;
    private ConnexionMySQL mysql;
    private Utilisateur utilisateur;

    @Override 
    public void init(){
        this.fenetre = new BorderPane();
        this.bRetour = new Button();
        this.bActualiser = new Button();
        this.bDeconnexion = new Button();
        this.bDeconnexion.setOnAction(new ControleurDeconnexion(this));
    }

    @Override
    public void start(Stage stage) {
        this.modeConnexion();
        stage.setTitle("Allo 45");
        stage.setScene(new Scene(this.fenetre));
        stage.setMaximized(true);
        stage.show();
    }

    public void titre(String nom, boolean retour, boolean actualiser, boolean deconnexion) {
        BorderPane titre = new BorderPane();
        HBox boutonsGauche = new HBox(5);
        if(retour){
            ImageView imageButtonRetour = new ImageView("boutonRetourImg.png");
            this.bRetour.setGraphic(imageButtonRetour);
            imageButtonRetour.setPreserveRatio(true);
            imageButtonRetour.setFitHeight(30);
            boutonsGauche.getChildren().add(this.bRetour);
        }
        
        if(actualiser){
            ImageView imageButtonActualiser = new ImageView("bouton-actualiser.png");
            this.bActualiser.setGraphic(imageButtonActualiser);
            imageButtonActualiser.setPreserveRatio(true);
            imageButtonActualiser.setFitHeight(30); 
            boutonsGauche.getChildren().add(this.bActualiser);
        }

        titre.setLeft(boutonsGauche);

        Text text = new Text(nom);
        text.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        titre.setCenter(text);

        if(deconnexion){
            ImageView imageButtonDeconnexion = new ImageView("ButtonDeconnexion.png");
            this.bDeconnexion.setGraphic(imageButtonDeconnexion);
            imageButtonDeconnexion.setPreserveRatio(true);
            imageButtonDeconnexion.setFitHeight(30);
            titre.setRight(this.bDeconnexion);            
        }
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height= (int) dimension.getHeight();
        int width= (int) dimension.getWidth();
        this.fenetre.setStyle("-fx-background-image: url('fondPageConnexion.jpg'); -fx-background-size:"+width+"px"+height+"px; fx-background-repeat: no-repeat; ");
        titre.setPadding(new Insets(40,20,40,20));
        this.fenetre.setTop(titre);
    }

    public void modeConcepteurAccueil() {
        AppliConcepteurAccueil laFenetre = new AppliConcepteurAccueil(this);
        this.fenetre.setCenter(laFenetre);
        this.titre("Bienvenue " + this.utilisateur.getNom()+ " " + this.utilisateur.getPrenom(), false, true, true);
        this.bActualiser.setOnAction(new ControleurActualiserConcepteur(laFenetre));
    }

    public void modeConnexion() {
        this.fenetre.setCenter(new AppliConnexion(this));
        this.titre("Connexion", false, false, false);
    }

    public void modeConcepteurChoisirTypeQuestion(Questionnaire questionnaire) {
        AppliConcepteurChoisirTypeQuestion laFenetre = new AppliConcepteurChoisirTypeQuestion(this, questionnaire);
        this.fenetre.setCenter(laFenetre);
        this.titre("Choisir une question", true, false, true);
        this.bRetour.setOnAction(new ControleurRetourConcepteur(laFenetre));
    }

    public void modeConcepteurCreerQuestion(Questionnaire questionnaire,String type) {
        AppliConcepteurCreerQuestion laFenetre = new AppliConcepteurCreerQuestion(this, questionnaire, type);
        this.fenetre.setCenter(laFenetre);
        this.titre("Créer un nouvelle question", true, false, true);
        this.bRetour.setOnAction(new ControleurRetourConcepteur(laFenetre));
    }

    public void modeConcepteurCreerQuestion(Questionnaire questionnaire, Question question) {
        AppliConcepteurCreerQuestion laFenetre = new AppliConcepteurCreerQuestion(this, questionnaire, question);
        this.fenetre.setCenter(laFenetre);
        this.titre("Modifier une question", true, false, true);
        this.bRetour.setOnAction(new ControleurRetourConcepteur(laFenetre));
    }

    public void modeSondeurAccueil() {
        AppliSondeurAccueil laFenetre = new AppliSondeurAccueil(this);
        this.fenetre.setCenter(laFenetre);
        this.titre("Bienvenue " + this.utilisateur.getNom()+ " " + this.utilisateur.getPrenom(), false, true, true);
        this.bActualiser.setOnAction(new ControleurActualiserSondeur(laFenetre));

    }

    public void modeSondeurRepondreQuestion(Questionnaire q) {
        AppliSondeurRepondreQuestion laFenetre = new AppliSondeurRepondreQuestion(this, q);
        this.fenetre.setCenter(laFenetre);
        this.titre("Sondage en cours", true, false, true);
        this.bRetour.setOnAction(new ControleurRetourSondeur(laFenetre));
    }

    public void modeCreerQuestionnaire() {
        AppliAttribuerEntrepriseNom laFenetre = new AppliAttribuerEntrepriseNom(this, null);
        this.fenetre.setCenter(laFenetre);
        this.titre("Créer questionnaire", true, false, true);
        this.bRetour.setOnAction(new ControleurRetourConcepteur(laFenetre));
    }

    public void modeCreerQuestionnaire(Questionnaire q) {
        AppliAttribuerEntrepriseNom laFenetre = new AppliAttribuerEntrepriseNom(this, q);
        this.fenetre.setCenter(laFenetre);
        this.titre("Créer questionnaire", true, false, true);
        this.bRetour.setOnAction(new ControleurRetourConcepteur(laFenetre));
    }

    public Pane getFenetreActuelle() {
        return (Pane) this.fenetre.getCenter();
    }

    public static Alert popUpAvertissement(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.YES, ButtonType.NO);
        alert.setTitle("Avertissement");
        alert.setHeaderText("Attention");
        return alert;
    }

    public static Alert popUpAvertissement(String titre, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.YES, ButtonType.NO);
        alert.setTitle("Avertissement");
        alert.setHeaderText(titre);
        return alert;
    }

    public static void main(String [] args){
        launch(args);
    }

    public ConnexionMySQL getMysql() {
        return mysql;
    }

    public void setMysql(ConnexionMySQL mysql) {
        this.mysql = mysql;
    }

    public static Alert popUpInformation(String titre, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message);
        alert.setTitle("Avertissement");
        alert.setHeaderText(titre);
        return alert;
    }

    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}