import java.util.Optional;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AppliSondeurRepondreQuestion extends BorderPane implements AppliSondeur{

    private AppliAllo45 appli;
    // private List<Question> lesQuestions;
    // private List<Sonde> lesSondes;
    private int numeroQuestion;
    //private Client entreprise;
    //private tempsAppel Chronometre;
    private BorderPane fenetreReponse;
    private Button sondeSuivant;
    private Button appeler;
    private Button sansReponse;
    private Button precedent;
    private Button suivant;
    private Questionnaire questionnaire;
    private Label instruction;
    private Sonde sondeActuel;
    private Label prenom;
    private Label nom;
    private Label age;

    //public AppliSoundeurRepondreQuestion(AppliAllo45 appli){
    //    this.appli=appli;}
    

    public AppliSondeurRepondreQuestion(AppliAllo45 appli, Questionnaire questionnaire) {
        super();
        this.questionnaire = questionnaire;
        //this.lesQuestions = new ArrayList();
        //this.lesSondes = new ArrayList();

        //this.tempsAppel = new Chronometre();
        //this.entreprise = new Client()
        this.appli = appli;
        this.numeroQuestion = 0;
        // Construction du graphe de scène
        BorderPane top = new BorderPane();
        VBox infos = new VBox();
        HBox appeler = new HBox();
        HBox centre = new HBox(20);
        VBox zoneReponse = new VBox();
        BorderPane bottom = new BorderPane();
        HBox navigation = new HBox();
        VBox zoneInfos = new VBox(10);
        VBox Info1 = new VBox();
        VBox Info3 = new VBox();
        VBox centreBas = new VBox();

        this.sondeSuivant = new Button("SONDÉ SUIVANT");
        this.sondeSuivant.setOnAction(new ControleurSondeSuivant(this));
        this.appeler = new Button("APPELER");
        this.sansReponse = new Button("SANS REPONSE"); 
        this.precedent = new Button("PRÉCÉDENT");
        this.precedent.setOnAction(new ControleurQuestionPrecedent(this));
        this.suivant = new Button("SUIVANT");
        this.suivant.setOnAction(new ControleurQuestionSuivante(this));
        this.sondeActuel = this.questionnaire.getPanel().getLesSondes().get(0);
        this.prenom = new Label("Prénom");
        this.nom = new Label("Nom");
        this.age = new Label("Âge");
        this.instruction = new Label("rien");

        
        Label entreprise = new Label("Entreprise concerné :");
        Text nomEntreprise = new Text(this.questionnaire.getClient().getRaisonSoc());
        
        Label nombreClientRepondu = new Label("Nombre de sondés ayant répondu :");
        Text nbCLient = new Text(this.questionnaire.getNbReponses());

        this.fenetreReponse = new BorderPane();
        top.setLeft(infos);
        top.setRight(this.sondeSuivant);
        infos.getChildren().addAll(prenom,nom,age,appeler);
        appeler.getChildren().addAll(this.appeler);
        centre.getChildren().addAll(zoneReponse,zoneInfos);
        zoneReponse.getChildren().addAll(instruction,this.fenetreReponse,bottom);
        navigation.getChildren().addAll(this.precedent,this.suivant);
        centreBas.getChildren().addAll(this.sansReponse,navigation);
        zoneInfos.getChildren().addAll(Info1,Info3);
        Info1.getChildren().addAll(entreprise,nomEntreprise);
        Info3.getChildren().addAll(nombreClientRepondu,nbCLient);
        bottom.setBottom(navigation);
        // bottom.setBottom(sondeSuivant);
        zoneReponse.setStyle("-fx-border-color: black; -fx-border-width:2px;");
        zoneInfos.setStyle("-fx-border-color: black; -fx-border-width:2px;");
        Info1.setStyle("-fx-border-color: black; -fx-border-width:1px;");
        Info3.setStyle("-fx-border-color: black; -fx-border-width:1px;");
        this.setTop(top);
        this.setCenter(centre);
        this.setPadding(new Insets(20));
        zoneInfos.setPadding(new Insets(10));
        zoneReponse.setPadding(new Insets(10));
        this.changerTypeFenetre();
        this.changerInfosSonde();

    }

    private void changerTypeFenetre() {
        switch(this.questionActuelle().getType()){
            case "u": this.fenetreReponse.setCenter(new SondeurUnique(this.questionActuelle())); break;
            case "m": this.fenetreReponse.setCenter(new SondeurMulti(this.questionActuelle())); break;
            case "n": this.fenetreReponse.setCenter(new sondeurNote(this.questionActuelle())); break;
            case "c": this.fenetreReponse.setCenter(new sondeurClassement(this.questionActuelle())); break;
            case "l": this.fenetreReponse.setCenter(new SondeurLibre(this.questionnaire, this.questionActuelle(), this.appli)); break;
        }
        this.instruction.setText(this.questionActuelle().getQuestion());
    }

    private void changerInfosSonde() {
        this.prenom.setText("Prénom : " + this.sondeActuel.getPrenom());
        this.nom.setText("Nom : " + this.sondeActuel.getNom());
        this.age.setText("Date de naissance : " + this.sondeActuel.getDateDeNaissance());
        this.activerBoutonChangerSonde(false);
    }

    private Question questionActuelle() {
        return this.questionnaire.getLesQuestions().get(this.numeroQuestion);
    }

    @Override
    public void retourArriere() {  
        Optional<ButtonType> reponse = this.appli.popUpAvertissement("Êtes-vous sur de retourner en arrière ?", "Vous n'avez pas terminé de remplir les questions.").showAndWait();
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            this.appli.modeSondeurAccueil();
        }
    }

    public void questionSuivant() {
        this.mettreReponse();
        if(this.numeroQuestion < this.questionnaire.getLesQuestions().size() - 1){
            this.numeroQuestion += 1;
            this.changerTypeFenetre();
        } else {
            this.activerBoutonPrecSuiv(false);
            this.activerBoutonChangerSonde(true);
            this.appli.popUpInformation("Fin des questions", "Vous avez atteint la dernière question, vous pouvez changer de sondé").showAndWait();
        }
    }

    public void questionPrecedent() {
        this.mettreReponse();
        if(0 < this.numeroQuestion){
            this.numeroQuestion -= 1;
            this.changerTypeFenetre();
        }
    }

    private void mettreReponse() {
        FenetreTypesReponses fenetre = (FenetreTypesReponses) this.fenetreReponse.getCenter();
        this.questionActuelle().setReponseDonne(fenetre.getReponse());
    }

    public void changerSonde() {
        try {
            RequeteSQL requete = new RequeteSQL(this.appli.getMysql());
            requete.enregistrerReponses(this.questionnaire, this.sondeActuel, this.appli.getUtilisateur());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.sondeActuel = this.questionnaire.getPanel().supprimerPremierSonde();
        if(this.sondeActuel != null) {
            this.changerInfosSonde();
            this.numeroQuestion = 0;
            this.changerTypeFenetre();
            this.activerBoutonPrecSuiv(true);
        } else {
            this.activerBoutonPrecSuiv(false);
            this.appli.popUpInformation("Fin du questionnaire", "Tous les sondés ont répondu au questionnaire").showAndWait();
            try {
                RequeteSQL requete = new RequeteSQL(this.appli.getMysql());
                requete.passerAnalyste(this.questionnaire);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void activerBoutonChangerSonde(boolean b) {
        this.sondeSuivant.setDisable(!b);
    }

    private void activerBoutonPrecSuiv(boolean b) {
        this.precedent.setDisable(!b);
        this.suivant.setDisable(!b);
    }
}