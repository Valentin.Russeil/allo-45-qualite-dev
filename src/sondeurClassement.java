import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class sondeurClassement extends FlowPane implements FenetreTypesReponses{
    private List<String> listeRep;
    private int maxClassement;
    private List<ComboBox<String>> listeCombo;

    // public sondeurReponseClassement(int maxClassement){
    //     this.maxClassement = maxClassement;
    // }

    public sondeurClassement(Question question){
        super();
        this.listeCombo = new ArrayList<>();
        this.listeRep = question.getLesReponses();
        
        maxClassement = 3;
        int clas = 0;
        for(int i=0;maxClassement>i;i++){
            HBox laBox = new HBox(5);
            ComboBox<String> box = new ComboBox<String>();
            listeCombo.add(box);
            clas++;
            String claString = String.valueOf(clas);
            Label claLabel = new Label(claString+"-") ; 
            for (String s:listeRep){
                box.getItems().add(s);
            }
            box.getSelectionModel().select(0);
            laBox.getChildren().addAll(claLabel,box);
            laBox.setAlignment(Pos.CENTER);
            this.getChildren().addAll(laBox);
        }
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setAlignment(Pos.CENTER);
    }

    @Override
    public String getReponse(){
        String listeReponseSelectionne = "";
        for (ComboBox<String> b:listeCombo){
            listeReponseSelectionne += b.getValue() + ";";
        }
        return listeReponseSelectionne+"\b ";       
    }
}