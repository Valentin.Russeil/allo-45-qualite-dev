import java.sql.SQLException;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

public class ControleurPublier implements EventHandler<ActionEvent> {

    private AppliAllo45 appliAllo45;
    private AppliConcepteurAccueil appliConcepteurAccueil;

    public ControleurPublier(AppliAllo45 appliAllo45, AppliConcepteurAccueil appliConcepteurAccueil) {
        this.appliAllo45 = appliAllo45;
        this.appliConcepteurAccueil = appliConcepteurAccueil;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        try {
            Questionnaire questionnaire = this.appliConcepteurAccueil.getQuestionnaireActuel();
            Optional<ButtonType> reponse = this.appliAllo45.popUpAvertissement("Publier une questionnaire", "Voulez-vous vraiment vous publier le questionnaire " + questionnaire.getNomQuestionnaire() + " ?\nVous ne pourrez plus la modifier à l'avenir").showAndWait();
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                RequeteSQL requete = new RequeteSQL(this.appliAllo45.getMysql());
                requete.publier(questionnaire);
                this.appliConcepteurAccueil.actualiser();
            }
        }
        catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
