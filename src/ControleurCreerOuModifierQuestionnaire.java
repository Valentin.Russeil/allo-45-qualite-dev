import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerOuModifierQuestionnaire implements EventHandler<ActionEvent>{
    
    private AppliAllo45 appli;
    private AppliAttribuerEntrepriseNom appliAttribuer;
    private Questionnaire questionnaire;
    
    public ControleurCreerOuModifierQuestionnaire(AppliAllo45 appli, AppliAttribuerEntrepriseNom appliAttribuer, Questionnaire questionnaire) {
        this.appli = appli;
        this.appliAttribuer = appliAttribuer;
        this.questionnaire = questionnaire;
    }

    @Override
    public void handle(ActionEvent arg0) {
        Questionnaire q = this.appliAttribuer.genererQuestionnaire();
        if(this.questionnaire == null){
            try {
                RequeteSQL requetes = new RequeteSQL(this.appli.getMysql());
                requetes.ajouterQuestionnaire(q);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            try {
                RequeteSQL requetes = new RequeteSQL(this.appli.getMysql());
                q.setNumero(this.questionnaire.getNumero());
                requetes.modifierQuestionnaire(q);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        
        this.appli.modeConcepteurAccueil();        
    }
}
