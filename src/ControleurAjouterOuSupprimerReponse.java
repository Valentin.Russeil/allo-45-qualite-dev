import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurAjouterOuSupprimerReponse implements EventHandler<ActionEvent> {

    private AppliConcepteurCreerQuestion concepteurCreerQuestion;


    public ControleurAjouterOuSupprimerReponse(AppliConcepteurCreerQuestion concepteurCreerQuestion){
        this.concepteurCreerQuestion = concepteurCreerQuestion;
    }

    @Override
    public void handle(ActionEvent event) {
        Button b = (Button) event.getSource();
        String txt = b.getText();
        if(txt.contains("AJOUTER")){
            String rep=this.concepteurCreerQuestion.getReponse();
            this.concepteurCreerQuestion.ajouterLaReponse(rep);
            this.concepteurCreerQuestion.effacerReponse();
        }
        if(txt.contains("SUPPRIMER")){
            this.concepteurCreerQuestion.supprimerElementComboBox();
        }
        
    }
}