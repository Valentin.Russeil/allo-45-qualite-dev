import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AppliConcepteurAccueil extends VBox implements AppliConcepteur{

    private AppliAllo45 appli;
    private TableView<Questionnaire> tableauSondage;
    private TableView<Question> tableauQuestionSondage;
    private TableColumn colonneQuestion;
    private TableColumn colonneQuestionnaire;
    private TableColumn colonneEntreprise;
    private Button buttonCreerQuestion;
    private Button buttonModifierQuestion;
    private Button buttonSupprimerQuestion;
    private Button buttonModifierQuestionnaire;
    private Button buttonCreerQuestionnaire;
    private Button buttonSupprimerQuestionnaire;
    private Button boutonPublier;
    private TableColumn colPanel;


    public void center() {
        this.tableauSondage = new TableView<>();
        this.tableauQuestionSondage = new TableView<>();
        VBox partieGauche = new VBox();
        partieGauche.setSpacing(5);
        partieGauche.setPadding(new Insets(20));

        // /**
        //  * Test d'insertion de donnée dans le TableView
        //  */
        // ObservableList<Questionanire> data = FXCollections.observableArrayList(
        //     new Sondage("Vélux", "Velulux"),
        //     new Sondage("Couleurs", "Colour"),
        //     new Sondage("Avis sur Iphone", "Apple"),
        //     new Sondage("Avis sur les ordinateurs", "IUTO")
        // );


        // colonneDateModif.setCellValueFactory(new PropertyValueFactory<Sondage, String>("datedemodification"));
        this.colonneQuestionnaire = new TableColumn<>("Questionnaire");
        
        // colonneQuestionnaire.setCellValueFactory(new PropertyValueFactory<Sondage, String>("questionnaire"));
        this.colonneEntreprise = new TableColumn<>("Entreprise");

        this.colPanel = new TableColumn<>("Panel");
        
        // colonneEntreprise.setCellValueFactory(new PropertyValueFactory<Sondage, String>("entreprise"));
        
        // this.tableauSondage.setItems(data);

        /**
         * Permet d'ajouter toutes les colonnes dans le TableView
         */
        this.tableauSondage.getColumns().addAll(colonneQuestionnaire, colonneEntreprise, colPanel);
        this.tableauSondage.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        colonneQuestionnaire.setMaxWidth( 1f * Integer.MAX_VALUE * 40 ); // 40% width
        colonneEntreprise.setMaxWidth( 1f * Integer.MAX_VALUE * 30 ); // 30% width
        colPanel.setMaxWidth( 1f * Integer.MAX_VALUE * 30 ); // 30% width



        HBox hboxButton = new HBox();
        hboxButton.setPadding(new Insets(20));
        hboxButton.setSpacing(300);
        hboxButton.setAlignment(Pos.CENTER);

        this.buttonCreerQuestionnaire = new Button("Créer");
        buttonCreerQuestionnaire.setTooltip(new Tooltip("Permet d'insérer un nouveau questionnaire"));
        buttonCreerQuestionnaire.setOnAction(new controleurCreerQuestionnaire(appli));
        this.buttonSupprimerQuestionnaire = new Button("Supprimer");
        buttonSupprimerQuestionnaire.setOnAction(new ControleurSupprimerQuestionnaire(this.appli,this));
        buttonSupprimerQuestionnaire.setTooltip(new Tooltip("Permet de supprimer le questionnaire sélectionné"));
        this.buttonModifierQuestionnaire = new Button("Modifier");
        buttonModifierQuestionnaire.setOnAction(new ControleurModifierQuestionnaire(this.appli,this));
        buttonModifierQuestionnaire.setTooltip(new Tooltip("Permet de modifier le questionnaire sélectionné"));
        this.boutonPublier = new Button("Publier");
        boutonPublier.setTooltip(new Tooltip("Permet de mettre le questionnaire à l'état prêt"));
        boutonPublier.setOnAction(new ControleurPublier(this.appli, this));

        hboxButton.getChildren().addAll(buttonCreerQuestionnaire, this.buttonModifierQuestionnaire, buttonSupprimerQuestionnaire, boutonPublier);

        partieGauche.getChildren().addAll(tableauSondage, hboxButton);

        VBox partieDroite = new VBox();
        partieDroite.setSpacing(5);
        partieDroite.setPadding(new Insets(20));

        /**
         * Test d'insertion de données
         */
        // ObservableList<Question> data2 = FXCollections.observableArrayList(
        //     new Question("11/06/22", "14/06/22", "Sur une échelle de 0 à 10\ncomment noteriez vous la\nla couleur des lampadaires\nde la ville ?", "Prêt"),
        //     new Question("12/06/22", "12/06/22", "Sur une échelle de 0 à 10\ncomment noteriez vous la\nla couleur de la marie ?", "En construction"),
        //     new Question("13/06/22", "13/06/22", "Parmi ces couleurs classez en\ntrois pour repeindre la façade de\nla mairie", "Prêt")
        // );

        /**
         * Création des colonnes du TableView
         */

       
        // colonneDateModif2.setCellValueFactory(new PropertyValueFactory<Sondage, String>("datedemodification"));
        this.colonneQuestion = new TableColumn<>("Question");
        // colonneQuestion.setCellValueFactory(new PropertyValueFactory<Question, String>("question"));

        // this.tableauQuestionSondage.setItems(data2);

        /**
         * Permet d'ajouter toutes les colonnes dans le TableView
         */
        this.tableauQuestionSondage.getColumns().addAll(colonneQuestion);
        this.tableauQuestionSondage.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        colonneQuestion.setMaxWidth( 1f * Integer.MAX_VALUE * 100 ); // 70% width

        HBox hboxQuestion = new HBox();
        hboxQuestion.setPadding(new Insets(20));
        hboxQuestion.setSpacing(250);
        hboxQuestion.setAlignment(Pos.CENTER);
        
        this.buttonCreerQuestion = new Button("Créer");
        buttonCreerQuestion.setOnAction(new ControleurQuestion(appli, this));
        buttonCreerQuestion.setTooltip(new Tooltip("Permet de rajouter une question dans le questionnaire"));
        this.buttonModifierQuestion = new Button("Modifier");
        buttonModifierQuestion.setOnAction(new ControleurQuestion(appli, this));
        buttonModifierQuestion.setTooltip(new Tooltip("Permet de modifier la question sélectionnée"));
        this.buttonSupprimerQuestion = new Button("Supprimer");
        buttonSupprimerQuestion.setOnAction(new ControleurQuestion(appli, this));
        buttonSupprimerQuestion.setTooltip(new Tooltip("Permet de supprimer la question sélectionnée"));
        
        hboxQuestion.getChildren().addAll(buttonCreerQuestion, buttonModifierQuestion, buttonSupprimerQuestion);

        partieDroite.getChildren().addAll(tableauQuestionSondage, hboxQuestion);


        this.getChildren().add(partieGauche);
        this.getChildren().add(partieDroite);

        this.tableauSondage.setOnMouseClicked(new ObservableSelectionQuestionnaireConcepteur(appli, this));
        this.tableauQuestionSondage.setOnMouseClicked(new ObservableSelectionQuestionConcepteur(appli, this));
    }


    // public AppliSondeurAccueil(Application appli, Map<Questionnaire, List<Question>> dicoQuestionnaire) {TODO}

    @Override
    public void retourArriere() {
        System.out.println("Rien");
    }

    public void actualiser() {
        this.afficherQuestionnaires();
        for (int i = 0; i<this.tableauQuestionSondage.getItems().size(); i++) {
            this.tableauQuestionSondage.getItems().clear(); 
        } 
        this.faireActiverBoutonQuestionnaire(false);
        this.faireActiverBoutonQuestion(false);
    }

    public Questionnaire getQuestionnaireActuel() {
        return (Questionnaire) this.tableauSondage.getSelectionModel().selectedItemProperty().get();
    }

    public Question getQuestionActuelle() {
        return (Question) this.tableauQuestionSondage.getSelectionModel().selectedItemProperty().get();

        
    }

    public void faireActiverBoutonQuestionnaire(boolean b) {
        this.buttonCreerQuestion.setDisable(!b);
        this.buttonModifierQuestionnaire.setDisable(!b);
        this.buttonSupprimerQuestionnaire.setDisable(!b);
        this.boutonPublier.setDisable(!b);
    }

    public void faireActiverBoutonQuestion(boolean b) {
        this.buttonModifierQuestion.setDisable(!b);
        this.buttonSupprimerQuestion.setDisable(!b);
    }

    // public void mettreLesQuestions() {TODO}

    public void afficherQuestions(Questionnaire q){
        ArrayList<Question> repTabQuestionnaire = q.getLesQuestions();
        ObservableList<Question> ob = FXCollections.observableArrayList(repTabQuestionnaire);
        this.colonneQuestion.setCellValueFactory(new PropertyValueFactory<Question, String>("question"));
        this.tableauQuestionSondage.setItems(ob);
    }

    public void afficherQuestionnaires(){
        try {
            RequeteSQL requetes = new RequeteSQL(appli.getMysql());
            ArrayList<Questionnaire> repTabQuestionnaire = requetes.questionnaireConcepteur();
            ObservableList<Questionnaire> ob = FXCollections.observableArrayList(repTabQuestionnaire);
            this.colonneQuestionnaire.setCellValueFactory(new PropertyValueFactory<Questionnaire, String>("nomQuestionnaire"));
            this.colonneEntreprise.setCellValueFactory(new PropertyValueFactory<Questionnaire, String>("client"));
            this.colPanel.setCellValueFactory(new PropertyValueFactory<Questionnaire, String>("panel"));
            this.tableauSondage.setItems(ob);
        } catch (Exception e) {
            System.out.println("Attention:"+e.getMessage());
        }
    }


    public AppliConcepteurAccueil(AppliAllo45 appli) {
        super();
        this.appli = appli;
        this.center();
        this.actualiser();
    }


    public void desactiverBoutonPublierSelonCondition() {
        if(this.getQuestionnaireActuel().getLesQuestions().size() == 0) {
            this.boutonPublier.setDisable(true);
        }
    }
}