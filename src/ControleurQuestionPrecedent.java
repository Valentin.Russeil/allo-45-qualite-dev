import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurQuestionPrecedent implements EventHandler<ActionEvent>{

    AppliSondeurRepondreQuestion appli;

    public ControleurQuestionPrecedent(AppliSondeurRepondreQuestion appli){
        this.appli=appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.questionPrecedent();      
    }
    
}
