import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class controleurCreerQuestionnaire implements EventHandler<ActionEvent>{
    
    private AppliAllo45 appliAllo45;

    public controleurCreerQuestionnaire(AppliAllo45 appli) {
        this.appliAllo45 = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appliAllo45.modeCreerQuestionnaire();
    }
}
