import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Slider;

public class ControlleurBouton implements EventHandler<ActionEvent> {
    private sondeurNote appli;
    private int numBouton;

    public ControlleurBouton(sondeurNote appli,int numBouton){
        this.appli = appli;
        this.numBouton = numBouton;
    }

    @Override
    public void handle(ActionEvent event){
        Slider slide =  this.appli.getSlider();
        if(this.numBouton == 1){
            String noteS = this.appli.getNote();
            int noteI = Integer.parseInt(noteS);
            slide.setValue(noteI-1);
        }
        if(this.numBouton == 2){
            String noteS = this.appli.getNote();
            int noteI = Integer.parseInt(noteS);
            slide.setValue(noteI+1);

        }
    }
}
