import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class QuestionClassement  extends GridPane {
    public QuestionClassement(AppliAllo45 appli, ComboBox<String> cTypeR, TextField tfAjoute,ComboBox<String> cConsulter, TextField classement, Button bAjouter, Button bSupprimer){
        super();

        Text typeR= new Text("Type de réponse : ");
        Text ajoute= new Text("Ajouter une réponse : ");
        Text max= new Text("Classement maximum : ");
        Text consulter= new Text("Consulter les réponse : ");

        this.add(typeR, 1, 1);
        this.add(ajoute, 1, 2);
        this.add(max, 1, 3);
        this.add(consulter, 1, 4);

        this.add(cTypeR, 2, 1);
        this.add(tfAjoute, 2, 2);
        this.add(classement, 2,3);
        this.add(cConsulter, 2, 4);

        this.add(bAjouter, 3, 2);
        this.add(bSupprimer, 3, 4);

        try{
            ConnexionMySQL mysql = appli.getMysql();
            RequeteSQL requete = new RequeteSQL(mysql);
            for(String rep : requete.lesTypesReponses()){
                cTypeR.getItems().addAll(rep);
            }
            cTypeR.getSelectionModel().select(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        this.setHgap(5);
        this.setVgap(10);
        this.setPadding(new Insets(20,20,20,20));
    }
}
