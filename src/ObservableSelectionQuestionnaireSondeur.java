import javafx.event.EventHandler;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

public class ObservableSelectionQuestionnaireSondeur implements EventHandler<MouseEvent>{

    private AppliAllo45 appli;
    private AppliSondeurAccueil fenetre;

    public ObservableSelectionQuestionnaireSondeur(AppliAllo45 appli, AppliSondeurAccueil fenetre){
        this.appli = appli;
        this.fenetre = fenetre;
    }

    @Override
    public void handle(MouseEvent event){
        this.fenetre.faireActiverBoutonLancer(true);
    }
}