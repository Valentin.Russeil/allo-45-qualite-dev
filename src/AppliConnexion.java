import java.awt.Dimension;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class AppliConnexion extends BorderPane{
    private TextField utilisateurTF;
    private PasswordField motDePassePF;
    private Text texteErreur;
    private Button boutonConnexion;
    private AppliAllo45 appli;
    private GridPane zoneConnexion;

    public AppliConnexion(AppliAllo45 appli) {
        super();
        this.texteErreur = new Text("Votre identifiant ou mot de passe est incorrect");
        this.zoneConnexion = new GridPane();
        this.appli = appli;
        this.utilisateurTF = new TextField();
        this.motDePassePF = new PasswordField();
        this.boutonConnexion = new Button("Login");
        this.boutonConnexion.setOnAction(new ControleurConnexion(this));
        VBox zoneImageEtTexteInstruction = new VBox();
        this.zoneConnexion = new GridPane();
        ImageView image = new ImageView(new Image("user.jpg"));
        Label texteInstruction = new Label("Entrez votre identifiant et votre mot de passe");
        Label identifiant = new Label("Identifiant : ");
        Label mdp = new Label("Mot De Passe : ");
        zoneImageEtTexteInstruction.getChildren().addAll(image,texteInstruction,this.zoneConnexion);

        this.zoneConnexion.add(identifiant,1,1);
        this.zoneConnexion.add(this.utilisateurTF,2,1);
        this.zoneConnexion.add(mdp,1,2);
        this.zoneConnexion.add(this.motDePassePF,2,2);
        this.zoneConnexion.add(this.boutonConnexion,3,3);

        image.setFitHeight(200);
        image.setFitWidth(175);

        this.zoneConnexion.setHgap(20);
        this.zoneConnexion.setVgap(10);

        zoneImageEtTexteInstruction.setSpacing(20);
     
        zoneImageEtTexteInstruction.setAlignment(Pos.CENTER);
        this.zoneConnexion.setAlignment(Pos.CENTER);
        
        this.setCenter(zoneImageEtTexteInstruction);
    }

    public String getUtilisateur(){
        return this.utilisateurTF.getText().toString();
    }

    public String getMotDePasse(){
        return this.motDePassePF.getText().toString();
    }

    public boolean seConnecter(String utilisateur,String mdp){
        try{
            ConnexionMySQL mysql = new ConnexionMySQL();
            mysql.connecter("russeil", "russeil");
            RequeteSQL requete = new RequeteSQL(mysql);
            Utilisateur user = requete.recupererUtilisateur(utilisateur, mdp);
            this.appli.setUtilisateur(user);
            appli.setMysql(mysql);
            switch (user.getRole()) {
                case 1 : appli.modeConcepteurAccueil(); break;
                case 2 : appli.modeSondeurAccueil(); break;
                case 3 : appli.popUpAvertissement("Attention", "La page de l'analyse n'existe pas").showAndWait(); return false;
            }
            return mysql.isConnecte();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public void faireActiverBoutonConnexion(boolean b){
        if(b){
            this.boutonConnexion.setDisable(false);
        }
        else{
            this.boutonConnexion.setDisable(true);
        }

    }

    public void mettreErreurImpossibleConnexion(){
        this.zoneConnexion.add(this.texteErreur, 1, 3, 1, 2);
    }

    public void enleverErreurImpossibleConnexion(){
        this.zoneConnexion.add(new Text(""), 1, 3, 1, 2);
    }

}