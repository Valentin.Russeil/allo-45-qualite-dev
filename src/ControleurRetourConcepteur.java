import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRetourConcepteur implements EventHandler<ActionEvent>{

    private AppliConcepteur fenetre;

    public ControleurRetourConcepteur(AppliConcepteur fenetre){
        this.fenetre = fenetre;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        fenetre.retourArriere();
    }

}