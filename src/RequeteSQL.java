import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequeteSQL{
    ConnexionMySQL laConnexion;
	Statement st;

	public RequeteSQL(ConnexionMySQL laConnexion) throws SQLException{
		this.laConnexion=laConnexion;
        this.st = this.laConnexion.createStatement();
	}

    public void ajouterQuestionnaire(Questionnaire q) throws SQLException{
        this.st.executeUpdate("insert into QUESTIONNAIRE(idQ, Titre, Etat, numC, idU, idPan) values (" + q.getNumero() + ", '" + q.getNomQuestionnaire() + "', 'C', "+ q.getClient().getNum() + ", " + 1 + ", " + q.getPanel().getNum() + ")");
    }

    public void ajouterClient(Client c) throws SQLException{
        this.st.executeUpdate("insert into CLIENT(numC, raisonSoc) values (" + c.getNum() + ", '" + c.getRaisonSoc() + "')");
    }

    public void modifierQuestionnaire(Questionnaire questionnaire) throws SQLException{
        st.executeUpdate("update QUESTIONNAIRE set Titre='"+questionnaire.getNomQuestionnaire()+"', idPan=" + questionnaire.getPanel().getNum() + " where idQ="+questionnaire.getNumero());
    }

    public List<Panel> lesPanels() throws SQLException{
        ResultSet rs = this.st.executeQuery("select idPan, nomPan from PANEL");
        List<Panel> res = new ArrayList<>();
        while (rs.next()) {
            res.add(new Panel(rs.getInt("idPan"), rs.getString("nomPan")));
        }
        return res;
    }

    public List<Client> lesEntreprises() throws SQLException{
        ResultSet rs = this.st.executeQuery("select numC, raisonSoc from CLIENT");
        List<Client> res = new ArrayList<>();
        while (rs.next()) {
            res.add(new Client(rs.getInt("numC"), rs.getString("raisonSoc")));
        }
        return res;
    }
    
    public int nbMaxQuestionnaire() throws SQLException{
        ResultSet rs = this.st.executeQuery("select max(idQ) FROM QUESTIONNAIRE");
        if(!rs.next()){
            return 0;
        }
        return rs.getInt("max(idQ)");
    }

    public ArrayList<Questionnaire> questionnaireConcepteur() throws SQLException{
        ResultSet rs = this.st.executeQuery("select idQ, Titre, Etat, numC, raisonSoc, numQ, texteQ, idPan, nomPan, idT, Valeur, MaxVal, typeReponse from QUESTIONNAIRE natural left join CLIENT natural left join QUESTION natural left join TYPEQUESTION natural left join VALPOSSIBLE natural left join PANEL where Etat = 'C'");        
        ArrayList<Questionnaire> res = new ArrayList<>();
        String type;
        int numQ;
        String question;
        Question laQuestion;
        while (rs.next()){
            int act = rs.getInt("idQ");
            Questionnaire q = new Questionnaire(rs.getInt("idQ"), rs.getString("Titre"), rs.getString("Etat"), new Client(rs.getInt("numC"), rs.getString("raisonSoc")), new Panel(rs.getInt("idPan"), rs.getString("nomPan")), "0");
            rs.previous();
            while(rs.next() && rs.getInt("idQ") == act){
                question = rs.getString("texteQ");
                if(question != null){
                    numQ = rs.getInt("numQ");
                    type = rs.getString("idT");
                    if(type.equals("n") || type.equals("c")){
                        laQuestion = new Question(rs.getInt("numQ"), question,type, rs.getInt("MaxVal"));
                    } else {
                        laQuestion = new Question(rs.getInt("numQ"), question,type);
                    }
                    do {
                        laQuestion.ajouterLaReponse(rs.getString("Valeur"));
                    }while(rs.next() && rs.getInt("idQ") == act && rs.getInt("numQ") == numQ && rs.getString("Valeur") != null);
                    q.ajouterQuestion(laQuestion);
                    rs.previous();
                }
            }
            rs.previous();
            res.add(q);
        }

        for(Questionnaire q : res) {
            this.ajouterLesSondes(q);
        }

        return res;
    }       

    /**
     * On ajoute les sondés qui n'ont pas été interrogés
     */
    private void ajouterLesSondes(Questionnaire questionnaire) throws SQLException {
        ResultSet rs = this.st.executeQuery("select numSond, nomSond, prenomSond, dateNaisSond, telephoneSond, idC from SONDE natural join CONSTITUER natural join CARACTERISTIQUE where idPan = " + questionnaire.getPanel().getNum() + " and numSond not in (select numSond from INTERROGER where idQ = " + questionnaire.getNumero() +")");
        while(rs.next()) {
            questionnaire.ajouterSonde(new Sonde(rs.getInt("numSond"), rs.getString("nomSond"), rs.getString("prenomSond"), rs.getString("dateNaisSond"), rs.getString("telephoneSond"), rs.getString("idC")));
        }
    }

    public ArrayList<Questionnaire> questionnaireSondeur() throws SQLException{
        ResultSet rs = this.st.executeQuery("select idQ, Titre, Etat, numC, raisonSoc, numQ, texteQ, idPan, nomPan, idT, Valeur, MaxVal, typeReponse from QUESTIONNAIRE natural left join CLIENT natural left join QUESTION natural left join TYPEQUESTION natural left join VALPOSSIBLE natural left join PANEL where Etat = 'S'");        
        ArrayList<Questionnaire> res = new ArrayList<>();
        String type;
        int numQ;
        String question;
        Question laQuestion;
        while (rs.next()){
            int act = rs.getInt("idQ");
            Questionnaire q = new Questionnaire(rs.getInt("idQ"), rs.getString("Titre"), rs.getString("Etat"), new Client(rs.getInt("numC"), rs.getString("raisonSoc")), new Panel(rs.getInt("idPan"), rs.getString("nomPan")), "0");
            ResultSet rsNbRep = this.st.executeQuery("select count(i.idQ) nbRep from INTERROGER i inner join QUESTIONNAIRE q on (i.idQ = q.idQ) where idPan = " + q.getPanel().getNum() + " and q.idQ = " + q.getNumero());
            if(rsNbRep.next()){
                q.setNbReponses(rsNbRep.getString("nbRep"));
            }
            rs.previous();
            while(rs.next() && rs.getInt("idQ") == act){
                question = rs.getString("texteQ");
                if(question != null){
                    numQ = rs.getInt("numQ");
                    type = rs.getString("idT");
                    if(type.equals("n") || type.equals("c")){
                        laQuestion = new Question(rs.getInt("numQ"), question,type, rs.getInt("MaxVal"));
                    } else {
                        laQuestion = new Question(rs.getInt("numQ"), question,type);
                    }
                    do {
                        laQuestion.ajouterLaReponse(rs.getString("Valeur"));
                    }while(rs.next() && rs.getInt("idQ") == act && rs.getInt("numQ") == numQ && rs.getString("Valeur") != null);
                    q.ajouterQuestion(laQuestion);
                    rs.previous();
                }
            }
            rs.previous();
            res.add(q);
        }

        for(Questionnaire q : res) {
            this.ajouterLesSondes(q);
        }

        return res;
    } 

    public List<String> lesTypesReponses() throws SQLException{
        ResultSet rs = this.st.executeQuery("select distinct typeReponse from TYPEQUESTION");
        List<String> res = new ArrayList<>();
        while (rs.next()){
            res.add(rs.getString("typeReponse"));
        }
        return res;
    }

    public int nbMaxQuestion(Questionnaire q) throws SQLException{
        ResultSet rs = this.st.executeQuery("select max(numQ) FROM QUESTION where idQ = "+q.getNumero());
        if(rs.next()){
            return rs.getInt("max(numQ)");
        }
        return 0;
    }

    public int nbMaxValeur(Questionnaire questionnaire, Question question) throws SQLException{
        ResultSet rs = this.st.executeQuery("select max(idV) FROM VALPOSSIBLE where idQ = " + questionnaire.getNumero() + " and numQ = " + question.getNumero());
        if(rs.next()){
            return rs.getInt("max(idV)");
        }
        return 0;
    }
    
    public void modifierUneQuestion(Questionnaire questionnaire, Question question) throws SQLException {
        st = laConnexion.createStatement(); 
        if(question.getInfoSupp() != -1){
            System.out.println("1");
            st.executeUpdate("update QUESTION set MaxVal="+question.getInfoSupp()+"where idQ="+questionnaire.getNumero() + " and numQ=" + question.getNumero());
        }
        
        if(!question.getType().equals("n")){
            st.executeUpdate("delete from VALPOSSIBLE where idQ=" + questionnaire.getNumero() + " and numQ=" + question.getNumero());
            int maxi = 0;
            for(String rep : question.getLesReponses()){
                maxi = this.nbMaxValeur(questionnaire, question);
                st.executeQuery("insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values (" + questionnaire.getNumero()+","+question.getNumero() + "," + (maxi + 1) + ",'" + rep +"')");
            }
        }
        st.executeUpdate("update QUESTION set texteQ='"+question.getQuestion()+"' where idQ="+questionnaire.getNumero() + " and numQ=" + question.getNumero());
    }

    public void enregistrerNouvelleQuestion(Questionnaire questionnaire, Question question) throws SQLException {
        st = laConnexion.createStatement(); 
        if(question.getInfoSupp() == -1){
            st.executeQuery("insert into QUESTION(idQ, numQ, texteQ, MaxVal, idT) values (" + questionnaire.getNumero()+","+question.getNumero()+",'"+question.getQuestion()+"',NULL,'"+question.getType()+"')");
        }
        else{
            st.executeQuery("insert into QUESTION(idQ, numQ, texteQ, MaxVal, idT) values (" + questionnaire.getNumero()+","+question.getNumero()+",'"+question.getQuestion()+"',"+question.getInfoSupp()+",'"+question.getType()+"')");
        }

        int maxi = 0;
        for(String rep : question.getLesReponses()){
            maxi = this.nbMaxValeur(questionnaire, question);
            st.executeQuery("insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values (" + questionnaire.getNumero()+","+question.getNumero() + "," + (maxi + 1) + ",'" + rep +"')");
        }
    }

    public void supprimerQuestionnaire(Questionnaire questionnaire) throws SQLException {
        this.st.executeUpdate("delete from QUESTIONNAIRE where idQ=" + questionnaire.getNumero());
    }

    public void supprimerQuestion(Questionnaire questionnaire, Question question) throws SQLException {
        this.st.executeUpdate("delete from QUESTION where idQ=" + questionnaire.getNumero() + " and numQ=" + question.getNumero());
    }

    public int nbMaxEntreprise() throws SQLException {
        ResultSet rs = this.st.executeQuery("select max(numC) FROM CLIENT");
        if(!rs.next()){
            return 0;
        }
        return rs.getInt("max(numC)");
    }

    public void publier(Questionnaire questionnaire) throws SQLException {
        this.st.executeUpdate("update QUESTIONNAIRE set Etat= 'S' where idQ=" + questionnaire.getNumero());
    }

    public void ajouterReponseLibre(Questionnaire questionnaire, Question question, String rep) throws SQLException {
        int maxi = 0;
        maxi = this.nbMaxValeur(questionnaire, question);
        st.executeQuery("insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values (" + questionnaire.getNumero()+","+question.getNumero() + "," + (maxi + 1) + ",'" + rep +"')");
    }

    public void passerAnalyste(Questionnaire questionnaire) throws SQLException {
        this.st.executeUpdate("update QUESTIONNAIRE set Etat= 'A' where idQ=" + questionnaire.getNumero());
    }

    public void enregistrerReponses(Questionnaire questionnaire, Sonde sondeActuel, Utilisateur utilisateur) {
        try {
            st.executeQuery("insert into INTERROGER(idU, numSond, idQ) values (" + utilisateur.getIdU() + ", " + sondeActuel.getNumero() +", " + questionnaire.getNumero() + ")");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        for(Question question : questionnaire.getLesQuestions()){
            try {
                st.executeQuery("insert into REPONDRE(idQ, numQ, idC, valeur) values (" + questionnaire.getNumero() + ", " + question.getNumero() +", '" + sondeActuel.getIdCaracteristique() + "', '" + question.getReponseDonne() + "')");
            } catch (Exception e) {
                System.out.println("Problème de la BD, on ajoute pas la réponse (doublon idC)");
            }
        }
    }

    public Utilisateur recupererUtilisateur(String utilisateur, String mdp) throws SQLException{
        ResultSet rs = this.st.executeQuery("select nomU, prenomU, idU, idR from UTILISATEUR natural join ROLEUTIL where login='" + utilisateur + "' and motDePasse='" + mdp + "'");
        rs.next();
        return new Utilisateur(rs.getInt("idU"), rs.getString("nomU"), rs.getString("prenomU"), rs.getInt("idR"));
    }
}